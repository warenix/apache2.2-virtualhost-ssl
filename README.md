## Build docker image

```sh
sh build.sh
```

## Run container

```
sh run.sh
```

## Generate self signed cert for 2 domains

- green.itrc.org.hk
- yellow.itrc.org.hk

```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/yellow.key -out /etc/apache2/ssl/yellow.crt
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/green.key -out /etc/apache2/ssl/green.crt
```


## Setup http virtualhost


/etc/apache2/sites-enabled/001-yellow.itrc.org.hk.conf

```
<virtualhost *:80>
servername yellow.itrc.org.hk
documentroot /www/yellow.itrc.org.hk

directoryindex index.html 

<directory "/www/yellow.itrc.org.hk">  

  options -indexes +followsymlinks  

  allowoverride all  

  require all granted 

 </directory>  

</virtualhost>
```



/etc/apache2/sites-enabled/001-green.itrc.org.hk.conf

```
<virtualhost *:80>
servername green.itrc.org.hk
documentroot /www/green.itrc.org.hk

directoryindex index.html 

<directory "/www/green.itrc.org.hk">  

  options -indexes +followsymlinks  

  allowoverride all  

  require all granted 

 </directory>  

</virtualhost>
```

## Create document root

```
mkdir -p /www/green.itrc.org.hk /www/yellow.itrc.org.hk
```


Add /www/yellow.itrc.org.hk/index.html

```
<body style="background:yellow">
</body>
```

Add /www/green.itrc.org.hk/index.html

```
<body style="background:green">
</body>
```



## Add self signed ssl

Append to /etc/apache2/sites-enabled/001-yellow.itrc.org.hk.conf

```
<IfModule mod_ssl.c>
    <VirtualHost *:443>
	servername yellow.itrc.org.hk
        DocumentRoot /www/yellow.itrc.org.hk

	directoryindex index.html 

	<directory "/www/yellow.itrc.org.hk">  

	  options -indexes +followsymlinks  

	  allowoverride all  

	  require all granted 

	 </directory>  

        SSLEngine on
        SSLCertificateFile /etc/apache2/ssl/yellow.crt
        SSLCertificateKeyFile /etc/apache2/ssl/yellow.key
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
                        SSLOptions +StdEnvVars
        </Directory>
        BrowserMatch "MSIE [2-6]" \
                        nokeepalive ssl-unclean-shutdown \
                        downgrade-1.0 force-response-1.0
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
    </VirtualHost>
</IfModule>

```


Append to /etc/apache2/sites-enabled/001-green.itrc.org.hk.conf

```
<IfModule mod_ssl.c>
    <VirtualHost *:443>
	servername green.itrc.org.hk
        DocumentRoot /www/green.itrc.org.hk

	directoryindex index.html 

	<directory "/www/green.itrc.org.hk">  

	  options -indexes +followsymlinks  

	  allowoverride all  

	  require all granted 

	 </directory>  

        SSLEngine on
        SSLCertificateFile /etc/apache2/ssl/green.crt
        SSLCertificateKeyFile /etc/apache2/ssl/green.key
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
                        SSLOptions +StdEnvVars
        </Directory>
        BrowserMatch "MSIE [2-6]" \
                        nokeepalive ssl-unclean-shutdown \
                        downgrade-1.0 force-response-1.0
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
    </VirtualHost>
</IfModule>

```
